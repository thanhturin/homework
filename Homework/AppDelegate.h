//
//  AppDelegate.h
//  Homework
//
//  Created by Thanh Turin on 1/21/16.
//  Copyright © 2016 Thanh Turin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

