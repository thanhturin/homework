//
//  DirectionsViewController.h
//  Homework
//
//  Created by Thanh Turin on 1/21/16.
//  Copyright © 2016 Thanh Turin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface DirectionsViewController : UIViewController

@property (nonatomic, assign) CLLocationCoordinate2D currentPoint;
@property (nonatomic, assign) CLLocationCoordinate2D startPoint;
@property (nonatomic, assign) CLLocationCoordinate2D endPoint;

@property (nonatomic, strong) UITextField *startTextField;
@property (nonatomic, strong) UITextField *endTextField;

@property (nonatomic, strong) GMSMarker *startMarker;
@property (nonatomic, strong) GMSMarker *endMarker;

@property (nonatomic, strong) NSMutableArray *listMarkers;
@property (nonatomic, strong) NSMutableArray *listPositions;

@end
