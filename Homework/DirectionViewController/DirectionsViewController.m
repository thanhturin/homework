//
//  DirectionsViewController.m
//  Homework
//
//  Created by Thanh Turin on 1/21/16.
//  Copyright © 2016 Thanh Turin. All rights reserved.
//

#import "DirectionsViewController.h"
#import "SuggestedTableViewCell.h"
#import "CustomAlertView.h"
#import "MDDirectionService.h"
#import "AMTumblrHud.h"
#import "SMCalloutView.h"
#import "Reachability.h"

#define SCREEN_W            [UIScreen mainScreen].bounds.size.width
#define SCREEN_H            [UIScreen mainScreen].bounds.size.height
#define RGBColor(r, g, b)   [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0]
#define GetWidth(v)         v.frame.size.width
#define GetHeight(v)        v.frame.size.height
#define GetOriginX(v)       v.frame.origin.x
#define GetOriginY(v)       v.frame.origin.y

typedef NS_ENUM(NSInteger, TextFieldChoose) {
    kNotRegister = 0,
    kStartTextField,
    kEndTextField,
};

@interface DirectionsViewController () <UITextFieldDelegate, GMSMapViewDelegate, UITableViewDataSource, UITableViewDelegate, GMSAutocompleteFetcherDelegate, CLLocationManagerDelegate>

@end

@implementation DirectionsViewController {
    GMSMapView *mapView;
    BOOL isFirstLoad;
    CLGeocoder *geoCoder;
    
    UIView *controlView;
    TextFieldChoose currentTextField;
    
    GMSAutocompleteFetcher *fetcher;
    UITableView *suggestedTableView;
    
    NSMutableArray *suggestedModels;
    
    
    UIButton *switchButton;
    UIButton *routeButton;
    
    GMSPolyline *polyline;
    SMCalloutView *calloutView;
    AMTumblrHud *tumblrHUD;
    UIView *emptyCalloutView;
    
    BOOL isHide;
    UIView *currentPositionView;
    CLPlacemark *currentPlace;
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CustomAlertView *errorAlert;
    
    BOOL isConnectedToInternet;
    CGFloat kbHeight;
}

#pragma mark - LifeCycle

- (instancetype)init {
    if (self = [super init]) {
        // Create source for draw route
        self.listMarkers = [NSMutableArray new];
        self.listPositions = [NSMutableArray new];
        geocoder = [[CLGeocoder alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.startPoint = CLLocationCoordinate2DMake(0,0);
    self.navigationController.navigationBar.hidden = YES;
    
    // Create Map View
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:11.153920
                                                            longitude:106.360617
                                                                 zoom:12];
    
    mapView = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    mapView.settings.compassButton = NO;
    mapView.settings.myLocationButton = YES;
    mapView.delegate = self;
    mapView.myLocationEnabled = YES;
    
    // Listen to the myLocation property of GMSMapView.
    [mapView addObserver:self
              forKeyPath:@"myLocation"
                 options:NSKeyValueObservingOptionNew
                 context:NULL];
    
    [self.view addSubview:mapView];
    
    CGFloat controlViewHeight = 30;
    CGFloat controlViewWidth = SCREEN_W - 20;
    controlView = [[UIView alloc] initWithFrame:CGRectMake(10, 25, controlViewWidth, 100 + controlViewHeight)];
    [controlView setBackgroundColor:[UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0]];
    [controlView.layer setCornerRadius:3.0];
    [self.view addSubview:controlView];
    
    routeButton = [self createControlButtonWithFrame:CGRectMake(0, 95, controlViewWidth/2, controlViewHeight) title:@"ROUTE"];
    [routeButton addTarget:self action:@selector(didTapRouteButton) forControlEvents:UIControlEventTouchUpInside];
    [controlView addSubview:routeButton];
    
    switchButton = [self createControlButtonWithFrame:CGRectMake(controlViewWidth/2, 95, controlViewWidth/2, controlViewHeight) title:@"SWITCH"];
    [switchButton addTarget:self action:@selector(didTapSwitchButton) forControlEvents:UIControlEventTouchUpInside];
    [controlView addSubview:switchButton];
    
    // Create search textfield
    CGFloat kTextFieldHeight = 40;
    CGFloat kTextFieldWidth = controlViewWidth - 10;
    
    self.startTextField = [self createSearchViewWithFrame:CGRectMake(5, 5, kTextFieldWidth, kTextFieldHeight) title:@"START:" color:RGBColor(90, 177, 26)];
    [controlView addSubview:self.startTextField];
    
    self.endTextField = [self createSearchViewWithFrame:CGRectMake(5, 50, kTextFieldWidth, kTextFieldHeight) title:@"END:" color:[UIColor grayColor]];
    [controlView addSubview:self.endTextField];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:controlView.bounds];
    controlView.layer.masksToBounds = NO;
    controlView.layer.shadowColor = [UIColor grayColor].CGColor;
    controlView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    controlView.layer.shadowOpacity = 0.4f;
    controlView.layer.shadowPath = shadowPath.CGPath;
    
    // Create bounds
    GMSVisibleRegion visibleRegion = mapView.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:visibleRegion.farLeft
                                                                       coordinate:visibleRegion.nearRight];
    // Create the fetcher.
    fetcher = [[GMSAutocompleteFetcher alloc] initWithBounds:bounds filter:nil];
    fetcher.delegate = self;
    
    // Create suggestion autocomplete result
    suggestedTableView = [UITableView new];
    suggestedTableView.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0];
    suggestedTableView.delegate = self;
    suggestedTableView.dataSource = self;
    suggestedTableView.hidden = YES;
    suggestedTableView.hidden = YES;
    
    suggestedModels = [NSMutableArray new];
    
    
    calloutView = [[SMCalloutView alloc] init];
    emptyCalloutView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    currentPositionView = [[UIView alloc] initWithFrame:CGRectMake(0 , 150, SCREEN_W, 50)];
    currentPositionView.backgroundColor = [UIColor whiteColor];
    UILabel *currentTextLabel = [[UILabel alloc] initWithFrame:currentPositionView.bounds];
    currentTextLabel.textAlignment = NSTextAlignmentCenter;
    currentTextLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    [currentTextLabel setText:@"GET CURRENT LOCATION"];
    
    [currentPositionView addSubview:currentTextLabel];
    
    UIImageView *currentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 30, 30)];
    [currentImageView setImage:[UIImage imageNamed:@"current"]];
    [currentPositionView addSubview:currentImageView];
    
    UIButton *overlayButton = [[UIButton alloc] initWithFrame:currentPositionView.bounds];
    [overlayButton addTarget:self action:@selector(didTapCurrentPosition) forControlEvents:UIControlEventTouchUpInside];
    [currentPositionView addSubview:overlayButton];
    
    currentPositionView.hidden = YES;
    [self.view addSubview:currentPositionView];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // fetching current location start from here
    [locationManager startUpdatingLocation];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)dealloc {
    [mapView removeObserver:self
                 forKeyPath:@"myLocation"
                    context:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KVO keyboard showing

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    kbHeight = kbSize.height;
}

#pragma mark - Create view object

- (UIButton *)createControlButtonWithFrame:(CGRect)frame title:(NSString *)title {
    UIButton *controlButton = [[UIButton alloc] initWithFrame:frame];
    [controlButton setBackgroundColor:[UIColor clearColor]];
    [controlButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:title];
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    [titleString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0, titleString.length)];
    
    NSMutableAttributedString *titleStringHighlight = [[NSMutableAttributedString alloc] initWithString:title];
    [titleStringHighlight addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    [titleStringHighlight addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, titleString.length)];
    
    [controlButton setAttributedTitle:titleString forState:UIControlStateNormal];
    [controlButton setAttributedTitle:titleStringHighlight forState:UIControlStateHighlighted];
    return controlButton;
}

- (UITextField *)createSearchViewWithFrame:(CGRect)frame title:(NSString *)title color:(UIColor *)color {
    UITextField * searchTextField = [[UITextField alloc] initWithFrame:frame];
    searchTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    searchTextField.layer.borderWidth = 0;
    searchTextField.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0];
    searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    searchTextField.keyboardType = UIKeyboardTypeDefault;
    searchTextField.returnKeyType = UIReturnKeyDone;
    searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    searchTextField.font = [UIFont fontWithName:@"Helvetica" size:14];
    searchTextField.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
    
    UILabel *textFieldTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, frame.size.height)];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@.", title]];
    [string addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,string.length-1)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:12] range:NSMakeRange(0, string.length)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor clearColor] range:NSMakeRange(string.length-1,1)];
    textFieldTitle.attributedText = string;
    [textFieldTitle setTextAlignment:NSTextAlignmentRight];
    [textFieldTitle setBackgroundColor:[UIColor clearColor]];
    searchTextField.leftView = textFieldTitle;
    searchTextField.leftViewMode = UITextFieldViewModeAlways;
    
    [searchTextField addTarget:self
                        action:@selector(textFieldDidChange:)
              forControlEvents:UIControlEventEditingChanged];
    searchTextField.delegate = self;
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(51.0f, frame.size.height - 7.5, frame.size.width - 51, 1.0);
    bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
    [searchTextField.layer addSublayer:bottomBorder];
    
    return searchTextField;
}

#pragma mark - Button action

- (BOOL)checkZeroPoint:(CLLocationCoordinate2D)point {
    return (point.latitude == 0 && point.longitude == 0);
}

- (BOOL)didTapRouteButton {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        isConnectedToInternet = NO;
        if (!errorAlert) {
            errorAlert = [[CustomAlertView alloc] initWithTitle:@"Notification" message:@"There is NO internet connection! Please connect to using this application better!" delegate:self buttonTitles:@[@"Cancel"]];
            errorAlert.tag = 20;
            [self.startTextField resignFirstResponder];
            [self.endTextField resignFirstResponder];
            [errorAlert show];
        }
        return NO;
    }
    
    if (!self.startMarker || !self.endMarker || [self checkZeroPoint:self.startPoint] || [self checkZeroPoint:self.endPoint])
    {
        CustomAlertView *alertView = [[CustomAlertView alloc] initWithTitle:@"Notification" message:@"You must fill start place and end place first!\nTip: You can hold on the screen to create place quickly!)" delegate:self buttonTitles:@[@"Cancel"]];
        [alertView setTag:10];
        [alertView show];
        return NO;
    }
    
    if (self.startPoint.latitude == self.endPoint.latitude && self.startPoint.longitude == self.endPoint.longitude) {
        CustomAlertView *alertView = [[CustomAlertView alloc] initWithTitle:@"Notification" message:@"Your destination place is your start place! Have fun boy ;)" delegate:self buttonTitles:@[@"Cancel"]];
        [alertView setTag:10];
        [alertView show];
        return NO;
    }
    
    [self.startTextField resignFirstResponder];
    [self.endTextField resignFirstResponder];
    [suggestedTableView setHidden:YES];
    [suggestedTableView removeFromSuperview];
    
    [self.listPositions removeAllObjects];
    [self.listMarkers removeAllObjects];
    
    NSString *startPositionString = [[NSString alloc] initWithFormat:@"%f,%f", self.startPoint.latitude, self.startPoint.longitude];
    NSString *endPositionString = [[NSString alloc] initWithFormat:@"%f,%f", self.endPoint.latitude, self.endPoint.longitude];
    NSMutableArray *positions = [[NSMutableArray alloc] initWithObjects:startPositionString, endPositionString, nil];
    [self.listPositions addObject:positions];
    
    NSMutableArray *makers = [[NSMutableArray alloc] initWithObjects:self.startMarker, self.endMarker, nil];
    [self.listMarkers addObject:makers];
    
    for (int i = 0; i < 2;  i++) {
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, self.listPositions[0], nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters forKeys:keys];
        
        MDDirectionService *directionService=[[MDDirectionService alloc] init];
        [directionService setDirectionsQuery:query withSelector:@selector(addDirections:) withDelegate:self];
    }
    return YES;
}

- (void)didTapSwitchButton {
    [self.startTextField resignFirstResponder];
    [self.endTextField resignFirstResponder];
    [suggestedTableView setHidden:YES];
    [suggestedTableView removeFromSuperview];
    
    CLLocationCoordinate2D temp = self.startPoint;
    self.startPoint = self.endPoint;
    self.endPoint = temp;
    
    NSString *tempString = self.startTextField.text;
    self.startTextField.text = self.endTextField.text;
    self.endTextField.text = tempString;
    
    [self resetMarker];
    
    if (self.startMarker && self.endMarker && ![self checkZeroPoint:self.startPoint] && ![self checkZeroPoint:self.endPoint]) {
        [self updateCamera:@[self.startMarker, self.endMarker]];
    }
}

- (void)didTapCurrentPosition {
    suggestedTableView.hidden = YES;
    [suggestedTableView removeFromSuperview];
    
    if (!currentPlace) {
        CustomAlertView *alertView = [[CustomAlertView alloc] initWithTitle:@"Notification" message:@"Cannot detect your location.\nPlease try again!" delegate:self buttonTitles:@[@"Try Again", @"Cancel"]];
        alertView.tag = 10;
        [self.startTextField resignFirstResponder];
        [self.endTextField resignFirstResponder];
        [alertView show];
    }
    else {
        if (currentTextField == kStartTextField) {
            self.startTextField.text = currentPlace.name;
            [self.startTextField resignFirstResponder];
            self.startPoint = currentPlace.location.coordinate;
            [self resetMarker];
            [self updateCamera:@[self.startMarker]];
        } else {
            self.endTextField.text = currentPlace.name;
            [self.endTextField resignFirstResponder];
            self.endPoint = currentPlace.location.coordinate;
            [self resetMarker];
            [self updateCamera:@[self.endMarker]];
        }
    }
}

#pragma mark - Updating camera

- (void)updateCamera:(NSArray *)markers {
    GMSCoordinateBounds *bounds;
    for (GMSMarker *marker in markers) {
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:marker.position
                                                          coordinate:marker.position];
        }
        bounds = [bounds includingCoordinate:marker.position];
    }
    if (markers.count == 1) {
        GMSCameraUpdate *update = [GMSCameraUpdate setTarget:((GMSMarker *)markers[0]).position];
        
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat:0.4] forKey:kCATransactionAnimationDuration];
        [mapView animateWithCameraUpdate:update];
        [CATransaction commit];
        return;
    }
    else {
        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds
                                              withEdgeInsets:UIEdgeInsetsMake(200, 50, 50, 50)];
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat:0.5] forKey:kCATransactionAnimationDuration];
        [mapView animateWithCameraUpdate:update];
        [CATransaction commit];
    }
}

- (void)resetMarker {
    [mapView clear];
    if (![self checkZeroPoint:self.startPoint]) {
        self.startMarker = [GMSMarker markerWithPosition:self.startPoint];
        self.startMarker.draggable = YES;
        self.startMarker.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
        self.startMarker.appearAnimation = kGMSMarkerAnimationPop;
        self.startMarker.userData = @"start";
        self.startMarker.map = mapView;
    }
    if (![self checkZeroPoint:self.endPoint]) {
        self.endMarker = [GMSMarker markerWithPosition:self.endPoint];
        self.endMarker.draggable = YES;
        self.endMarker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        self.endMarker.appearAnimation = kGMSMarkerAnimationPop;
        self.endMarker.userData = @"end";
        self.endMarker.map = mapView;
    }
}

- (void)updateMarkerAnimate:(BOOL)animate {
    [mapView clear];
    if (![self checkZeroPoint:self.startPoint]) {
        if (!self.startMarker) {
            self.startMarker = [GMSMarker markerWithPosition:self.startPoint];
        } else {
            self.startMarker.position = self.startPoint;
        }
        self.startMarker.draggable = YES;
        self.startMarker.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
        self.startMarker.appearAnimation = kGMSMarkerAnimationPop;
        self.startMarker.userData = @"start";
        self.startMarker.map = mapView;
    }
    if (![self checkZeroPoint:self.endPoint]) {
        if (!self.endMarker) {
            self.endMarker = [GMSMarker markerWithPosition:self.endPoint];
        } else {
            self.endMarker.position = self.endPoint;
        }
        self.endMarker.draggable = YES;
        self.endMarker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        self.endMarker.appearAnimation = kGMSMarkerAnimationPop;
        self.endMarker.userData = @"end";
        self.endMarker.map = mapView;
    }
}

#pragma mark - CustomAlertViewDelegate

- (void)customAlertViewButtonClicked:(CustomAlertView*)alertView clickedButtonAtIndex:(int)indexButton {
    [alertView close];
    if (alertView.tag == 20) {
        errorAlert = nil;
        return;
    }
    if (alertView.tag != 10) {
        return;
    }
    if (indexButton == 0) {
        [locationManager startUpdatingLocation];
    }
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!isFirstLoad) {
        geoCoder = [CLGeocoder new];
        isFirstLoad = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        self.currentPoint = location.coordinate;
        mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                        zoom:14];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (suggestedModels && suggestedModels.count > 0) {
        return suggestedModels.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!suggestedModels || suggestedModels.count == 0) {
        UITableViewCell *cell = [[UITableViewCell alloc] init];
        cell.textLabel.text = @"Get your current location";
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        cell.imageView.image = [UIImage imageNamed:@"current_icon"];
        return cell;
    }
    
    SuggestedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[SuggestedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:@"Cell"];
    }
    if (suggestedModels.count > indexPath.row) {
        if (suggestedModels[indexPath.row]) {
            if (indexPath.row % 2 == 1) {
                [cell setBackgroundColor:RGBColor(240, 240, 240)];
            } else {
                [cell setBackgroundColor:[UIColor whiteColor]];
            }
            [cell updateData:suggestedModels[indexPath.row]];
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!suggestedModels || suggestedModels.count == 0) {
        return 40;
    } else {
        return 75;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!suggestedModels || suggestedModels.count == 0) {
        [self didTapCurrentPosition];
        return;
    }
    
    if (currentTextField == kStartTextField) {
        [self.startTextField setText:[suggestedModels[indexPath.row][@"Name"] string]];
        [self.startTextField resignFirstResponder];
        self.startPoint = ((CLLocation *)suggestedModels[indexPath.row][@"Coordinate"]).coordinate;
        [self resetMarker];
        [self updateCamera:@[self.startMarker]];
    }
    else {
        [self.endTextField setText:[suggestedModels[indexPath.row][@"Name"] string]];
        [self.endTextField resignFirstResponder];
        self.endPoint = ((CLLocation *)suggestedModels[indexPath.row][@"Coordinate"]).coordinate;
        [self resetMarker];
        [self updateCamera:@[self.endMarker]];
    }
    
    suggestedTableView.hidden = YES;
    [suggestedTableView removeFromSuperview];
    
}

#pragma mark - GMSAutocompleteFetcherDelegate

- (void)didAutocompleteWithPredictions:(NSArray *)predictions {
    dispatch_group_t getInfo = dispatch_group_create();
    
    [suggestedModels removeAllObjects];
    for (GMSAutocompletePrediction *prediction in predictions) {
        UIFont *regularFont = [UIFont systemFontOfSize:[UIFont labelFontSize]];
        UIFont *boldFont = [UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
        
        __block NSMutableAttributedString *bolded = [prediction.attributedPrimaryText mutableCopy];
        [bolded enumerateAttribute:kGMSAutocompleteMatchAttribute
                           inRange:NSMakeRange(0, bolded.length)
                           options:0
                        usingBlock:^(id value, NSRange range, BOOL *stop) {
                            UIFont *font = (value == nil) ? regularFont : boldFont;
                            [bolded addAttribute:NSFontAttributeName value:font range:range];
                        }];
        
        dispatch_group_enter(getInfo);
        [[GMSPlacesClient sharedClient] lookUpPlaceID:prediction.placeID callback:^(GMSPlace * _Nullable result, NSError * _Nullable error) {
            CLLocation *resultLocation = [[CLLocation alloc] initWithLatitude:result.coordinate.latitude longitude:result.coordinate.longitude];
            if (!bolded || !result.formattedAddress || !result.types || !resultLocation) {
                return ;
            }
            
            [suggestedModels addObject:[NSDictionary dictionaryWithObjects:@[bolded, result.formattedAddress, [result.types[0] uppercaseString], resultLocation] forKeys:@[@"Name", @"Detail", @"Type", @"Coordinate"]]];
            dispatch_group_leave(getInfo);
        }];
    }
    
    dispatch_group_notify(getInfo, dispatch_get_main_queue(), ^{
        [suggestedTableView reloadData];
        if (suggestedModels && suggestedModels.count > 0) {
            suggestedTableView.frame = CGRectMake(GetOriginX(controlView),
                                                  GetOriginY(controlView) + GetHeight(controlView) + 5,
                                                  GetWidth(controlView),
                                                  SCREEN_H - (GetOriginY(controlView) + GetHeight(controlView) + 5 + kbHeight));
        } else {
            suggestedTableView.frame = CGRectMake(GetOriginX(controlView),
                                                  GetOriginY(controlView) + GetHeight(controlView) + 5,
                                                  GetWidth(controlView),
                                                  40);
        }
        suggestedTableView.hidden = NO;
        [self.view addSubview:suggestedTableView];
    });
}

- (void)didFailAutocompleteWithError:(NSError *)error {
    if (error) {
        if (!errorAlert) {
            errorAlert = [[CustomAlertView alloc] initWithTitle:@"Notification" message:error.localizedDescription delegate:self buttonTitles:@[@"OK"]];
            errorAlert.tag = 20;
            [self.startTextField resignFirstResponder];
            [self.endTextField resignFirstResponder];
            [errorAlert show];
        }
    }
}

#pragma mark - GMSMarkerDelegate

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker {
    if (marker == self.startMarker) {
        self.startPoint = marker.position;
    } else {
        self.endPoint = marker.position;
    }
    [self updateMarkerAnimate:NO];
    if (self.startMarker && self.endMarker) {
        [self updateCamera:@[self.startMarker, self.endMarker]];
    }
    
    [UIView animateWithDuration:0.35 animations:^{
        CGRect frame = controlView.frame;
        frame.origin.y = 25;
        controlView.frame = frame;
    }];
    isHide = NO;
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker {
    [self updateMarkerAnimate:NO];
}

- (void)mapView:(GMSMapView *)_mapView didDragMarker:(GMSMarker *)marker {
    NSLog(@"marker dragged to location: %f,%f", marker.position.latitude, marker.position.longitude);
    
    CLLocation *loc = [[CLLocation alloc] initWithLatitude: marker.position.latitude longitude:marker.position.longitude];
    [geoCoder reverseGeocodeLocation:loc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        CLPlacemark *placemark = [placemarks lastObject];
        if (marker == self.startMarker) {
            self.startTextField.text = placemark.name;
        } else {
            self.endTextField.text = placemark.name;
        }
    }];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    if (!isHide) {
        [UIView animateWithDuration:0.35 animations:^{
            CGRect frame = controlView.frame;
            frame.origin.y = - frame.size.height;
            controlView.frame = frame;
            frame = suggestedTableView.frame;
            frame.origin.y = -frame.size.height;
            suggestedTableView.frame = frame;
            [self.startTextField resignFirstResponder];
            [self.endTextField resignFirstResponder];
        }];
        isHide = YES;
    } else {
        [UIView animateWithDuration:0.35 animations:^{
            CGRect frame = controlView.frame;
            frame.origin.y = 25;
            controlView.frame = frame;
        }];
        isHide = NO;
    }
}

- (void)mapView:(GMSMapView *)pMapView didChangeCameraPosition:(GMSCameraPosition *)position {
    if (pMapView.selectedMarker != nil && !calloutView.hidden) {
        CLLocationCoordinate2D anchor = [pMapView.selectedMarker position];
        
        CGPoint arrowPt = calloutView.backgroundView.arrowPoint;
        
        CGPoint pt = [pMapView.projection pointForCoordinate:anchor];
        pt.x -= arrowPt.x;
        pt.y -= arrowPt.y + 50;
        
        calloutView.frame = (CGRect) {.origin = pt, .size = calloutView.frame.size };
    } else {
        calloutView.hidden = YES;
    }
}

- (void)mapView:(GMSMapView *)_mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    [self.startTextField resignFirstResponder];
    [self.endTextField resignFirstResponder];
    suggestedTableView.hidden = YES;
    [suggestedTableView removeFromSuperview];
    
    if (self.startMarker && self.endMarker) {
        NSLog(@"%@ %@", NSStringFromCGPoint(CGPointMake(coordinate.latitude, coordinate.longitude)), NSStringFromCGPoint(CGPointMake(self.startMarker.position.latitude, self.startMarker.position.longitude)));
        
        CGPoint longpressPoint = [mapView.projection pointForCoordinate:coordinate];
        for (GMSMarker *marker in @[self.startMarker, self.endMarker]) {
            CLLocationCoordinate2D markerCoordinate = marker.position;
            CGPoint sectorPoint = [mapView.projection pointForCoordinate:markerCoordinate];
            NSLog(@"%@ %@", NSStringFromCGPoint(longpressPoint), NSStringFromCGPoint(sectorPoint));

            if (fabs(longpressPoint.x - sectorPoint.x) < 45 && fabs(longpressPoint.y - sectorPoint.y) < 45) { // 30.f ofc should be defined as constant
                // handle situation when touchpoint is too close to marker
                return;
            }
        }

    };
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        isConnectedToInternet = NO;
        if (!errorAlert) {
            errorAlert = [[CustomAlertView alloc] initWithTitle:@"Notification" message:@"There is NO internet connection! Please connect to using this application better!" delegate:self buttonTitles:@[@"Cancel"]];
            errorAlert.tag = 20;
            [self.startTextField resignFirstResponder];
            [self.endTextField resignFirstResponder];
            [errorAlert show];
            return;
        }
    }
    
    if (currentTextField == kNotRegister || currentTextField == kStartTextField) {
        [self getPlaceMarkWithPosition:coordinate success:^(CLPlacemark *responseObject) {
            self.startTextField.text = responseObject.name;
        } failure:^(NSError *error) {
            NSLog(@"Fail");
        }];
        
        self.startPoint = coordinate;
        [self resetMarker];
        if (self.startMarker && self.endMarker) {
            [self updateCamera:@[self.startMarker, self.endMarker]];
        }
    }
    else {
        [self getPlaceMarkWithPosition:coordinate success:^(CLPlacemark *responseObject) {
            self.endTextField.text = responseObject.name;
        } failure:^(NSError *error) {
            NSLog(@"Fail");
        }];
        
        self.endPoint = coordinate;
        [self resetMarker];
        
        if (self.startMarker && self.endMarker) {
            [self updateCamera:@[self.startMarker, self.endMarker]];
        }
    }
}

- (void)getPlaceMarkWithPosition:(CLLocationCoordinate2D)coordinate success:(void (^)(CLPlacemark *responseObject))success failure:(void (^)(NSError *error))failure {
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    [geoCoder reverseGeocodeLocation:loc completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (!error) {
            if (success) {
                success([placemarks lastObject]);
            }
        } else {
            if (failure) {
                failure(error);
            }
        }
    }];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (currentPlace)
        return;
    
    // after we have current coordinates, we use this method to fetch the information data of fetched coordinate
    [geocoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        [locationManager stopUpdatingLocation];
        if (!error) {
            CLPlacemark *placemark = [placemarks lastObject];
            currentPlace = placemark;
            if (currentTextField == kStartTextField) {
                [self.startTextField setText:currentPlace.name];
            }
            else if (currentTextField == kEndTextField) {
                [self.endTextField setText:currentPlace.name];
            }
        }
        else {
            Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
            NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
            if (networkStatus == NotReachable) {
                isConnectedToInternet = NO;
                if (!errorAlert) {
                    errorAlert = [[CustomAlertView alloc] initWithTitle:@"Notification" message:@"There is NO internet connection! Please connect to using this application better!" delegate:self buttonTitles:@[@"Cancel"]];
                    errorAlert.tag = 20;
                    [self.startTextField resignFirstResponder];
                    [self.endTextField resignFirstResponder];
                    [errorAlert show];
                    return ;
                }
            } else {
                isConnectedToInternet = YES;
                CustomAlertView *alertView = [[CustomAlertView alloc] initWithTitle:@"Notification" message:error.localizedDescription delegate:self buttonTitles:@[@"Try Again", @"Cancel"]];
                alertView.tag = 10;
                [self.startTextField resignFirstResponder];
                [self.endTextField resignFirstResponder];
                [alertView show];
                return;
            }
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"failed to fetch current location : %@", error);
}

#pragma mark - Route

- (void)addDirections:(NSDictionary *)json {
    NSDictionary *routes = json[@"routes"][0];
    NSDictionary *route = [routes objectForKey:@"overview_polyline"];
    NSString *overview_route = [route objectForKey:@"points"];
    
    GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
    
    polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeColor = [UIColor grayColor];
    
    polyline.strokeWidth = 5;
    polyline.geodesic = YES;
    
    polyline.map = mapView;
    
    [((AMTumblrHud *)tumblrHUD) hide];
    
    // animation
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:polyline.path];
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:0.5] forKey:kCATransactionAnimationDuration];
    // change the camera, set the zoom, whatever.  Just make sure to call the animate* method.
    [mapView animateWithCameraUpdate:update];
    CGRect frame = controlView.frame;
    frame.origin.y = - 150;
    controlView.frame = frame;
    isHide = YES;
    
    [CATransaction commit];
    return;
}

- (UIView *)mapView:(GMSMapView *)_mapView markerInfoWindow:(GMSMarker *)marker {
    CLLocationCoordinate2D anchor = marker.position;
    
    CGPoint point = [_mapView.projection pointForCoordinate:anchor];
    calloutView.title = marker.title;
    calloutView.calloutOffset = CGPointMake(0, -50.0f);
    calloutView.hidden = NO;
    
    CGRect calloutRect = CGRectZero;
    calloutRect.origin = point;
    calloutRect.size = CGSizeZero;
    
    [calloutView presentCalloutFromRect:calloutRect
                                 inView:_mapView
                      constrainedToView:_mapView
                               animated:YES];
    
    return emptyCalloutView;
}

#pragma mark - UITextFieldDelegate

- (void)changeTextColor:(UILabel *)label color:(UIColor *)color {
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", label.text]];
    [string addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, string.length-1)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica-Bold" size:12] range:NSMakeRange(0, string.length)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor clearColor] range:NSMakeRange(string.length-1,1)];
    label.attributedText = string;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [suggestedModels removeAllObjects];
    [suggestedTableView reloadData];
    suggestedTableView.frame = CGRectMake(GetOriginX(controlView),
                                          GetOriginY(controlView) + GetHeight(controlView) + 5,
                                          GetWidth(controlView),
                                          40);
    suggestedTableView.hidden = NO;
    [self.view addSubview:suggestedTableView];
    
    if (textField == self.startTextField) {
        currentTextField = kStartTextField;
        [self changeTextColor:(UILabel *)self.startTextField.leftView color:RGBColor(90, 177, 26)];
        [self changeTextColor:(UILabel *)self.endTextField.leftView color:[UIColor grayColor]];
    } else {
        currentTextField = kEndTextField;
        [self changeTextColor:(UILabel *)self.endTextField.leftView color:RGBColor(90, 177, 26)];
        [self changeTextColor:(UILabel *)self.startTextField.leftView color:[UIColor grayColor]];
    }

    return YES;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (textField.text.length == 0) {
        [suggestedModels removeAllObjects];
        [suggestedTableView reloadData];
        suggestedTableView.frame = CGRectMake(GetOriginX(controlView),
                                              GetOriginY(controlView) + GetHeight(controlView) + 5,
                                              GetWidth(controlView),
                                              40);
        return;
    }
    [fetcher sourceTextHasChanged:textField.text];
}

@end
