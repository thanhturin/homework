//
//  SuggestedTableViewCell.h
//  Homework
//
//  Created by Thanh Turin on 1/21/16.
//  Copyright © 2016 Thanh Turin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuggestedTableViewCell : UITableViewCell

- (void)updateData:(NSDictionary *)newData;

@end
