//
//  SuggestedTableViewCell.m
//  Homework
//
//  Created by Thanh Turin on 1/21/16.
//  Copyright © 2016 Thanh Turin. All rights reserved.
//

#import "SuggestedTableViewCell.h"

#define SCREEN_W        [UIScreen mainScreen].bounds.size.width
#define LABEL_HEIGHT    25

@implementation SuggestedTableViewCell {
    UILabel *nameLabel;
    UILabel *detailLabel;
    UILabel *type;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, SCREEN_W - 20, LABEL_HEIGHT)];
        [nameLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        [nameLabel setTextColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0]];

        type = [[UILabel alloc] initWithFrame:CGRectMake(10, LABEL_HEIGHT, SCREEN_W - 20, LABEL_HEIGHT)];
        [type setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
        [type setTextColor:[UIColor colorWithRed:59.0/255.0 green:89.0/255.0 blue:152.0/255.0 alpha:1.0]];
        
        detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, LABEL_HEIGHT * 2, SCREEN_W - 20, LABEL_HEIGHT)];
        [detailLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        [detailLabel setTextColor:[UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0]];
        [self addSubview:nameLabel];
        [self addSubview:type];
        [self addSubview:detailLabel];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateData:(NSDictionary *)newData {
    [nameLabel setAttributedText:newData[@"Name"]];
    [type setText:newData[@"Type"]];
    [detailLabel setText:newData[@"Detail"]];
}

@end
