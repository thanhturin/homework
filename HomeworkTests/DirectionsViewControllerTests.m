//
//  DirectionsViewControllerTests.m
//  Homework
//
//  Created by Thanh Turin on 1/25/16.
//  Copyright © 2016 Thanh Turin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DirectionsViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Reachability.h"

#define RGBColor(r, g, b)   [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0]

@interface DirectionsViewController (UnitTest) <UITextFieldDelegate>

- (void)didTapSwitchButton;

- (BOOL)didTapRouteButton;

- (void)getPlaceMarkWithPosition:(CLLocationCoordinate2D)coordinate success:(void (^)(CLPlacemark *responseObject))success failure:(void (^)(NSError *error))failure;

@end

@interface DirectionsViewControllerTests : XCTestCase

@property (nonatomic) DirectionsViewController *vcToTest;

@end

@implementation DirectionsViewControllerTests

- (void)setUp {
    [super setUp];
    self.vcToTest = [DirectionsViewController new];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceGetPlaceByCoordinate {
    [self measureBlock:^{
        [self.vcToTest getPlaceMarkWithPosition:CLLocationCoordinate2DMake(11.153920, 106.360617) success:nil failure:nil];
    }];
}

- (void)testPerformanceRouteAction {
    // This is an example of a performance test case.
    [self measureBlock:^{
        self.vcToTest.startPoint = CLLocationCoordinate2DMake(10, 15);
        self.vcToTest.endPoint = CLLocationCoordinate2DMake(20, 25);
        self.vcToTest.startMarker = [GMSMarker markerWithPosition:self.vcToTest.startPoint];
        self.vcToTest.endMarker = [GMSMarker markerWithPosition:self.vcToTest.endPoint];
        [self.vcToTest didTapRouteButton];
    }];
}

- (BOOL)comparePoints:(CLLocationCoordinate2D)point1 point2:(CLLocationCoordinate2D)point2 {
    if (point1.latitude == point2.latitude && point1.longitude == point2.longitude) {
        return YES;
    }
    return NO;
}

- (void)testSwitch_CaseNormal {
    self.vcToTest.startPoint = CLLocationCoordinate2DMake(10, 15);
    self.vcToTest.endPoint = CLLocationCoordinate2DMake(20, 25);
    self.vcToTest.startTextField = [UITextField new];
    self.vcToTest.startTextField.text = @"Cho Ben Thanh";
    self.vcToTest.endTextField = [UITextField new];
    self.vcToTest.endTextField.text = @"Dai hoc Bach Khoa";
    
    CLLocationCoordinate2D expectStartPoint = CLLocationCoordinate2DMake(20, 25);
    CLLocationCoordinate2D expectEndPoint = CLLocationCoordinate2DMake(10, 15);
    NSString *expectStartText = @"Dai hoc Bach Khoa";
    NSString *expectEndText = @"Cho Ben Thanh";
//    UIColor *expectStartColor = ((UILabel *)self.vcToTest.endTextField.leftView).textColor;
//    UIColor *expectEndColor = ((UILabel *)self.vcToTest.startTextField.leftView).textColor;
    
    [self.vcToTest didTapSwitchButton];
    XCTAssertTrue([self comparePoints:self.vcToTest.startPoint point2:expectStartPoint], @"Switch point bug");
    XCTAssertTrue([self comparePoints:self.vcToTest.endPoint point2:expectEndPoint], @"Switch point bug");
    XCTAssertEqualObjects(self.vcToTest.startTextField.text, expectStartText);
    XCTAssertEqualObjects(self.vcToTest.endTextField.text, expectEndText);
}

- (void)testSwitch_CaseAllPointNull {
    CLLocationCoordinate2D expectStartPoint = CLLocationCoordinate2DMake(0, 0);
    CLLocationCoordinate2D expectEndPoint = CLLocationCoordinate2DMake(0, 0);
    
    [self.vcToTest didTapSwitchButton];
    XCTAssertTrue([self comparePoints:self.vcToTest.startPoint point2:expectStartPoint], @"Switch point bug");
    XCTAssertTrue([self comparePoints:self.vcToTest.endPoint point2:expectEndPoint], @"Switch point bug");
}

- (void)testSwitch_CaseStartPointNull {
    self.vcToTest.startPoint = CLLocationCoordinate2DMake(10, 15);
    
    CLLocationCoordinate2D expectStartPoint = CLLocationCoordinate2DMake(0, 0);
    CLLocationCoordinate2D expectEndPoint = CLLocationCoordinate2DMake(10, 15);
    
    [self.vcToTest didTapSwitchButton];
    XCTAssertTrue([self comparePoints:self.vcToTest.startPoint point2:expectStartPoint], @"Switch point bug");
    XCTAssertTrue([self comparePoints:self.vcToTest.endPoint point2:expectEndPoint], @"Switch point bug");
}

- (void)testSwitch_CaseEndPointNull {
    self.vcToTest.endPoint = CLLocationCoordinate2DMake(10, 15);
    
    CLLocationCoordinate2D expectStartPoint = CLLocationCoordinate2DMake(10, 15);
    CLLocationCoordinate2D expectEndPoint = CLLocationCoordinate2DMake(0, 0);
    
    [self.vcToTest didTapSwitchButton];
    XCTAssertTrue([self comparePoints:self.vcToTest.startPoint point2:expectStartPoint], @"Switch point bug");
    XCTAssertTrue([self comparePoints:self.vcToTest.endPoint point2:expectEndPoint], @"Switch point bug");
}

- (NetworkStatus)checkingNetwork {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    return networkStatus;
}

- (void)testRouteButton_CaseNormal {
    if ([self checkingNetwork] == NotReachable) {
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    } else {
        self.vcToTest.startPoint = CLLocationCoordinate2DMake(10, 15);
        self.vcToTest.endPoint = CLLocationCoordinate2DMake(20, 25);
        self.vcToTest.startMarker = [GMSMarker markerWithPosition:self.vcToTest.startPoint];
        self.vcToTest.endMarker = [GMSMarker markerWithPosition:self.vcToTest.endPoint];
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertTrue(result);
    }
}

- (void)testRouteButton_CaseNullAllMarker {
    if ([self checkingNetwork] == NotReachable) {
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    } else {
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    }
}

- (void)testRouteButton_CaseStartMakerNull {
    if ([self checkingNetwork] == NotReachable) {
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    } else {
        self.vcToTest.endPoint = CLLocationCoordinate2DMake(20, 25);
        self.vcToTest.endMarker = [GMSMarker markerWithPosition:self.vcToTest.endPoint];
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    }
}

- (void)testRouteButton_CaseEndMarkerNull {
    if ([self checkingNetwork] == NotReachable) {
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    } else {
        self.vcToTest.startPoint = CLLocationCoordinate2DMake(10, 15);
        self.vcToTest.startMarker = [GMSMarker markerWithPosition:self.vcToTest.startPoint];
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    }
}

- (void)testRouteButton_CaseEqualMarker {
    if ([self checkingNetwork] == NotReachable) {
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    } else {
        self.vcToTest.startPoint = CLLocationCoordinate2DMake(10, 15);
        self.vcToTest.endPoint = CLLocationCoordinate2DMake(10, 15);
        self.vcToTest.startMarker = [GMSMarker markerWithPosition:self.vcToTest.startPoint];
        self.vcToTest.endMarker = [GMSMarker markerWithPosition:self.vcToTest.endPoint];
        BOOL result = [self.vcToTest didTapRouteButton];
        XCTAssertFalse(result);
    }
}

@end
